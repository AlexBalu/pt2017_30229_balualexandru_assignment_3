package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import model.Order;

public class OrderDAO {
	   private static final Logger logger = Logger.getLogger(OrderDAO.class);
	    /** The query for find by id. */
	    private static final String FIND_BY_ID = "SELECT id_client,id_produs,cantitate FROM orders WHERE id_order = ?";
	    /** The query for creation. */
	    private static final String CREATE_QUERY = "INSERT INTO orders (id_order,id_client,id_produs,cantitate) VALUES (?,?,?,?)";
	    /** The query for read. */
	    private static final String READ_QUERY = "SELECT * FROM orders WHERE id_order = ?";
	    /** The query for update. */
	    private static final String UPDATE_QUERY = "UPDATE orders SET id_client=?, id_produs=?,cantitate=? WHERE id_order = ?";
	    /** The query for delete. */
	    private static final String DELETE_QUERY = "DELETE FROM orders WHERE id_order = ?";
	    
	    private ConnectionFactory c = new ConnectionFactory();
	    
	    public Order findById(int orderId) {
			Order toReturn = null;
			Connection conn=null;
		//	Connection dbConnection = ConnectionFactory.getConnection();
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			
			try {
				conn = c.createConnection();
				findStatement = conn.prepareStatement(FIND_BY_ID);
				findStatement.setLong(1, orderId);
				rs = findStatement.executeQuery();
				rs.next();
				int id_client = rs.getInt("id_client");
				int id_produs = rs.getInt("id_produs");
				int cantitate = rs.getInt("cantitate");
				toReturn = new Order(orderId,id_client,id_produs,cantitate);
				
			} catch (SQLException e) {
				System.out.println("OrderDAO:findById " + e.getMessage());
				//logger.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
			} finally {
				 try {
		                rs.close();
		            } catch (Exception rse) {
		               // logger.error(rse.getMessage());
		            }
		            try {
		                findStatement.close();
		            } catch (Exception sse) {
		               // logger.error(sse.getMessage());
		            }
		            try {
		                conn.close();
		            } catch (Exception cse) {
		             //   logger.error(cse.getMessage());
		            }
			}
			return toReturn;
		}
	    
	    public int create(Order order) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
	            preparedStatement.setInt(1, order.getId_order());
	            preparedStatement.setInt(2, order.getId_client());
	            preparedStatement.setInt(3, order.getId_produs());
	            preparedStatement.setInt(4, order.getCantitate());
	            preparedStatement.executeUpdate();
	           result = preparedStatement.getGeneratedKeys();
	 
	            if (result.next() && result != null) {
	                return result.getInt(1);
	            } else {
	                return -1;
	            }
	        } catch (SQLException e) {
	        	System.out.println("Eroare la create in OrderDAO"+e.getMessage());
	            //logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	               // logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	               // logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	             //   logger.error(cse.getMessage());
	            }
	        }
	 
	        return -1;
	    }
	 
	    public Order read(int id) {
	        Order order = null;
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(READ_QUERY);
	            preparedStatement.setInt(1,id);
	            preparedStatement.execute();
	            result = preparedStatement.getResultSet();
	 
	            if (result.next() && result != null) {
	                order = new Order(result.getInt(1),result.getInt(2),result.getInt(3),result.getInt(4));
	           
	            } else {
	                // TODO
	            	System.out.println("Result NULL in OrderDAO");
	            }
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	                logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	 
	        return order;
	    }
	 
	    public boolean update(Order order) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(UPDATE_QUERY);
	            
	            
	            preparedStatement.setInt(1, order.getId_client());
	            preparedStatement.setInt(2, order.getId_produs());
	            preparedStatement.setInt(3, order.getCantitate());
	            preparedStatement.setInt(4, order.getId_order());
	            preparedStatement.executeUpdate();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
	 
	    public boolean delete(int id) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(DELETE_QUERY);
	            preparedStatement.setInt(1, id);
	            preparedStatement.execute();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
	
}
