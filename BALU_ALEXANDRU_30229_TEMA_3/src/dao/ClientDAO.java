package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import java.util.logging.Logger;
import org.apache.log4j.Logger;

import model.Client;
import model.Produs;


public class ClientDAO implements ClientDAOInt{
	    private static final Logger logger = Logger.getLogger(ClientDAO.class);
	    /** The query for find by id. */
	    private static final String FIND_BY_ID = "SELECT nume,varsta FROM clients WHERE id_client = ?";
	    /** The query for creation. */
	    private static final String CREATE_QUERY = "INSERT INTO clients (id_client,nume,varsta) VALUES (?,?,?)";
	    /** The query for read. */
	    private static final String READ_QUERY = "SELECT id_client,nume,varsta FROM clients WHERE id_client = ?";
	    /** The query for update. */
	    private static final String UPDATE_QUERY = "UPDATE clients SET nume=? , varsta=? WHERE id_client = ?";
	    /** The query for delete. */
	    private static final String DELETE_QUERY = "DELETE FROM clients WHERE id_client = ?";
	    /** The query for select all. */
	    private static final String SELECTALL_QUERY = "SELECT * FROM clients";
	    
	    private ConnectionFactory c = new ConnectionFactory();
	    
	    
		public List<Client> selectAll() {
	    	Connection conn=null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			List<Client> list = new ArrayList<Client>();
			
			String query = SELECTALL_QUERY;
			try {
				conn = c.createConnection();
				statement = conn.prepareStatement(query);
				resultSet = statement.executeQuery();
				if(resultSet==null)
					System.out.println("resultSet null");
				try {
					while (resultSet.next()) {
						System.out.println(resultSet.getInt(1)+" "+ resultSet.getString(2)+" "+ resultSet.getInt(3));
					  list.add(new Client(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3)));
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				} 
				return list;
				
			} catch (SQLException e) {
				System.out.println("DAO:findById " + e.getMessage());
			} finally {
				 try {
		                resultSet.close();
		            } catch (Exception rse) {
		            	System.out.println("ClientDAO1 "+rse.getMessage());
		               // logger.error(rse.getMessage());
		            }
		            try {
		                statement.close();
		            } catch (Exception sse) {
		            	System.out.println("ClientDAO2 "+sse.getMessage());
		               // logger.error(sse.getMessage());
		            }
		            try {
		                conn.close();
		            } catch (Exception cse) {
		            	System.out.println("ClientDAO3 "+cse.getMessage());
		             //   logger.error(cse.getMessage());
		            }
			}
			return null;
		}
	    
	    
	    public Client findById(int clientId) {
			Client toReturn = null;
			Connection conn=null;
		//	Connection dbConnection = ConnectionFactory.getConnection();
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			
			try {
				conn = c.createConnection();
				findStatement = conn.prepareStatement(FIND_BY_ID);
				findStatement.setLong(1, clientId);
				rs = findStatement.executeQuery();
				rs.next();
				//int id = rs.getInt("id_client");
				String nume = rs.getString("nume");
				int varsta = rs.getInt("varsta");
				toReturn = new Client(clientId, nume, varsta);
				
			} catch (SQLException e) {
				System.out.println("ClientDAO:findById " + e.getMessage());
				//logger.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
			} finally {
				 try {
		                rs.close();
		            } catch (Exception rse) {
		               // logger.error(rse.getMessage());
		            }
		            try {
		                findStatement.close();
		            } catch (Exception sse) {
		               // logger.error(sse.getMessage());
		            }
		            try {
		                conn.close();
		            } catch (Exception cse) {
		             //   logger.error(cse.getMessage());
		            }
			}
			return toReturn;
		}
	    
	    public int create(Client client) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
	            preparedStatement.setInt(1, client.getId());
	            preparedStatement.setString(2, client.getNume());
	            preparedStatement.setInt(3, client.getAge());
	            preparedStatement.executeUpdate();
	           result = preparedStatement.getGeneratedKeys();
	 
	            if (result.next() && result != null) {
	                return result.getInt(1);
	            } else {
	                return -1;
	            }
	        } catch (SQLException e) {
	        	System.out.println("Eroare la create in ClientDAO"+e.getMessage());
	            //logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	               // logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	               // logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	             //   logger.error(cse.getMessage());
	            }
	        }
	 
	        return -1;
	    }
	 
	    public Client read(int id) {
	        Client client = null;
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(READ_QUERY);
	            preparedStatement.setInt(1,id);
	            preparedStatement.execute();
	            result = preparedStatement.getResultSet();
	 
	            if (result.next() && result != null) {
	                client = new Client(result.getInt(1),result.getString(2),result.getInt(3));
	                
	            } else {
	                // TODO
	            	System.out.println("Result NULL in ClientDAO");
	            }
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	                logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	 
	        return client;
	    }
	 
	    public boolean update(Client client) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(UPDATE_QUERY);
	            
	            preparedStatement.setString(1, client.getNume());
	            preparedStatement.setInt(2, client.getAge());
	            preparedStatement.setInt(3, client.getId());
	            preparedStatement.executeUpdate();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
	 
	    public boolean delete(int id) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(DELETE_QUERY);
	            preparedStatement.setInt(1, id);
	            preparedStatement.execute();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
	
}
