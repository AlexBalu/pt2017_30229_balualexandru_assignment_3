package dao;


public abstract class DAOFactory {

 
    public abstract ClientDAO getClientDAO();
    public abstract OrderDAO getOrdersDAO();
    public abstract ProducatorDAO getProducatorDAO();
    public abstract ProdusDAO getProdusDAO();
 

    public static DAOFactory getDAOFactory() {
         return new MysqlDAOFactory();
       }
   
}
