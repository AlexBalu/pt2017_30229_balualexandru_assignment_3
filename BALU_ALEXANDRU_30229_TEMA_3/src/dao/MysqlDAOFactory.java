package dao;

public class MysqlDAOFactory extends DAOFactory{
	  @Override
	    public  ClientDAO getClientDAO() {
	        return new ClientDAO();
	    }
	 
	    @Override
	    public OrderDAO getOrdersDAO() {
	        return new OrderDAO();
	    }
	    
	    @Override
	    public ProducatorDAO getProducatorDAO() {
	        return new ProducatorDAO();
	    }
	    
	    @Override
	    public ProdusDAO getProdusDAO() {
	        return new ProdusDAO();
	    }
	
	
}
