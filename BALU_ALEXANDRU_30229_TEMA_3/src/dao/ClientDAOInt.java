package dao;
import model.Client;

public interface ClientDAOInt {
	
	/**
	 * DAO-Interface for the different clientDAO-implementations. Defines the
	 * CRUD-operations.
	 */

	    /** Creates a client and returns the id. */
	    public int create(Client client);
	 
	    /** Receives a client by given id. */
	    public Client read(int id);
	 
	    /** Updates an existing client. */
	    public boolean update(Client client);
	 
	    /** Deletes a client by id. */
	    public boolean delete(int id);
}
