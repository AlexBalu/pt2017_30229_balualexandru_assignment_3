package model;

public class Producator {
private int id_producator;
private String nume_producator;
private int id_produs;


public Producator(int id_producator, String nume_producator, int id_produs) {
	// TODO Auto-generated constructor stub
	this.id_producator = id_producator;
	this.nume_producator = nume_producator;
	this.id_produs = id_produs;
}


public int getId_producator() {
	return id_producator;
}
public void setId_producator(int id_producator) {
	this.id_producator = id_producator;
}

public String getNume_producator() {
	return nume_producator;
}
public void setNume_producator(String nume_producator) {
	this.nume_producator = nume_producator;
}
public int getId_produs() {
	return id_produs;
}
public void setId_produs(int id_produs) {
	this.id_produs = id_produs;
}
}
