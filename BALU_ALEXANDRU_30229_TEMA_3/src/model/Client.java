package model;

public class Client {
	private int id_client;
	private String nume;
	private int varsta;
	
	public Client(int clientId, String name, int age) {
		// TODO Auto-generated constructor stub
		this.id_client = clientId;
		this.nume = name;
		this.varsta = age;
	}

	public int getId()
	{
		return this.id_client;
	}
	
	public String getNume()
	{
		return this.nume;
	}
	
	public int getAge()
	{
		return this.varsta;
	}
	
	public void setId(int v)
	{
	this.id_client = v;	
	}
	
	public void setNume(String n)
	{
		this.nume = n;
	}
	
	public void setVarsta(int v)
	{
		this.varsta = v;
	}

}
