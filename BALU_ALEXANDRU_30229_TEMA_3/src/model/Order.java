package model;

public class Order {
private int id_order;
private int id_client;
private int id_produs;
private int cantitate;


public Order(int id_order, int id_client, int id_produs, int cantitate) {
	// TODO Auto-generated constructor stub
	this.id_order = id_order;
	this.id_client = id_client;
	this.id_produs = id_produs;
	this.cantitate = cantitate;
}



public int getId_order() {
	return id_order;
}
public void setId_order(int id_order) {
	this.id_order = id_order;
}

public int getId_client() {
	return id_client;
}
public void setId_client(int id_client) {
	this.id_client = id_client;
}


public int getId_produs() {
	return id_produs;
}
public void setId_produs(int id_produs) {
	this.id_produs = id_produs;
}


public int getCantitate() {
	return cantitate;
}
public void setCantitate(int cantitate) {
	this.cantitate = cantitate;
}








}
