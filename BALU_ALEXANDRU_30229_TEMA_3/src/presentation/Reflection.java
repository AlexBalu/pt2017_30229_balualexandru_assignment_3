package presentation;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;

public class Reflection {
	public static JTable createJTable(List<Object> listaDeObiecte,Object[] atribute) {
		//declaram o noua matrice de obiecte Object
		Object [][] v = new Object[listaDeObiecte.size()][atribute.length];
		int i=0,j=0;
		
		//definim matricea de obiecte, pentru fiecare obiect din lista de obiecte
		//luam fiecare atribut in parte si il punem in matrice la locul corespunzator.
		for(Object it : listaDeObiecte){
			for (Field field : it.getClass().getDeclaredFields()) {
				field.setAccessible(true); 
				Object obj;
				
				try {
					obj = field.get(it);
					v[i][j]=obj;
					j++;
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}			
			}
			i++;
			j=0;
		}
		
		JTable t = new JTable(v,atribute);
		return t;
	}
}
