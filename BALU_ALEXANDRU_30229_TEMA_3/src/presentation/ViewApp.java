package presentation;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProdusBLL;
import model.Client;
import model.Order;
import model.Produs;

public class ViewApp {

	private JFrame frmTema;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_21;

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewApp window = new ViewApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public ViewApp() {
		
			initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTema = new JFrame();
		frmTema.setTitle("Tema3");
		frmTema.getContentPane().setBackground(Color.BLACK);
		frmTema.setBackground(Color.LIGHT_GRAY);
		frmTema.setBounds(100, 100, 1200, 800);
		frmTema.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTema.getContentPane().setLayout(null);
		
		JLabel lblIdclient_1 = new JLabel("id_client");
		lblIdclient_1.setForeground(Color.WHITE);
		lblIdclient_1.setBounds(33, 36, 46, 14);
		frmTema.getContentPane().add(lblIdclient_1);
		
		JLabel lblNumeclient = new JLabel("nume_client");
		lblNumeclient.setForeground(Color.WHITE);
		lblNumeclient.setBounds(33, 61, 64, 14);
		frmTema.getContentPane().add(lblNumeclient);
		
		JLabel lblVarsta = new JLabel("varsta");
		lblVarsta.setForeground(Color.WHITE);
		lblVarsta.setBounds(33, 86, 46, 14);
		frmTema.getContentPane().add(lblVarsta);
		
		textField = new JTextField();
		textField.setBounds(107, 36, 86, 20);
		frmTema.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(107, 61, 86, 20);
		frmTema.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(107, 86, 86, 20);
		frmTema.getContentPane().add(textField_2);
		
		JButton btnAdaugaClient = new JButton("Adauga Client");
		btnAdaugaClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			ClientBLL cbll = new ClientBLL();
			Client c = new Client(Integer.parseInt(textField.getText()) ,textField_1.getText(),Integer.parseInt(textField_2.getText()));
			cbll.insertClient(c);
			}
		});
		btnAdaugaClient.setBounds(33, 120, 104, 23);
		frmTema.getContentPane().add(btnAdaugaClient);
		
		JLabel label = new JLabel("id_client");
		label.setForeground(Color.WHITE);
		label.setBounds(253, 36, 46, 14);
		frmTema.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("nume_client");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(253, 61, 64, 14);
		frmTema.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("varsta");
		label_2.setForeground(Color.WHITE);
		label_2.setBounds(253, 86, 46, 14);
		frmTema.getContentPane().add(label_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(327, 36, 86, 20);
		frmTema.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(327, 61, 86, 20);
		frmTema.getContentPane().add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(327, 86, 86, 20);
		frmTema.getContentPane().add(textField_5);
		
		JButton btnEditClient = new JButton("Edit Client");
		btnEditClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientBLL cbll = new ClientBLL();
				Client c = new Client(Integer.parseInt(textField_3.getText()) ,textField_4.getText(),Integer.parseInt(textField_5.getText()));
				cbll.updateClient(c);
				
			}
		});
		btnEditClient.setBounds(253, 120, 104, 23);
		frmTema.getContentPane().add(btnEditClient);
		
		JLabel label_3 = new JLabel("id_client");
		label_3.setForeground(Color.WHITE);
		label_3.setBounds(457, 36, 46, 14);
		frmTema.getContentPane().add(label_3);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(515, 33, 86, 20);
		frmTema.getContentPane().add(textField_6);
		
		JButton btnDeleteClient = new JButton("Delete Client");
		btnDeleteClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientBLL cbll = new ClientBLL();
				cbll.deleteClient(Integer.parseInt(textField_6.getText()));
			}
		});
		btnDeleteClient.setBounds(457, 61, 104, 23);
		frmTema.getContentPane().add(btnDeleteClient);
		
		JButton btnViewClients = new JButton("View Clients");
		btnViewClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewClients vc = new ViewClients();
			}
		});
		btnViewClients.setBounds(457, 120, 104, 23);
		frmTema.getContentPane().add(btnViewClients);
		
		JLabel lblIdorder = new JLabel("id_produs");
		lblIdorder.setForeground(Color.WHITE);
		lblIdorder.setBounds(33, 186, 64, 14);
		frmTema.getContentPane().add(lblIdorder);
		
		JLabel lblIdclient = new JLabel("nume");
		lblIdclient.setForeground(Color.WHITE);
		lblIdclient.setBounds(33, 211, 64, 14);
		frmTema.getContentPane().add(lblIdclient);
		
		JLabel lblIdprodus = new JLabel("cantitate");
		lblIdprodus.setForeground(Color.WHITE);
		lblIdprodus.setBounds(33, 236, 64, 14);
		frmTema.getContentPane().add(lblIdprodus);
		
		JLabel lblCantitate = new JLabel("pret");
		lblCantitate.setForeground(Color.WHITE);
		lblCantitate.setBounds(33, 261, 46, 14);
		frmTema.getContentPane().add(lblCantitate);
		
		JLabel lblIdproducator = new JLabel("id_producator");
		lblIdproducator.setForeground(Color.WHITE);
		lblIdproducator.setBounds(33, 292, 64, 14);
		frmTema.getContentPane().add(lblIdproducator);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(107, 186, 86, 20);
		frmTema.getContentPane().add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(107, 211, 86, 20);
		frmTema.getContentPane().add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(107, 236, 86, 20);
		frmTema.getContentPane().add(textField_9);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(107, 261, 86, 20);
		frmTema.getContentPane().add(textField_10);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(107, 289, 86, 20);
		frmTema.getContentPane().add(textField_11);
		
		JButton btnAdaugaComanda = new JButton("Adauga Produs");
		btnAdaugaComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProdusBLL pbll = new ProdusBLL();
				Produs p = new Produs(Integer.parseInt(textField_7.getText()),textField_8.getText(),Integer.parseInt(textField_9.getText()),Integer.parseInt(textField_10.getText()),Integer.parseInt(textField_11.getText()));
				pbll.insertProdus(p);
			}
		});
		btnAdaugaComanda.setBounds(33, 336, 128, 23);
		frmTema.getContentPane().add(btnAdaugaComanda);
		
		JLabel label_4 = new JLabel("id_produs");
		label_4.setForeground(Color.WHITE);
		label_4.setBounds(253, 186, 64, 14);
		frmTema.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("nume");
		label_5.setForeground(Color.WHITE);
		label_5.setBounds(253, 211, 64, 14);
		frmTema.getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("cantitate");
		label_6.setForeground(Color.WHITE);
		label_6.setBounds(253, 236, 64, 14);
		frmTema.getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("pret");
		label_7.setForeground(Color.WHITE);
		label_7.setBounds(253, 261, 46, 14);
		frmTema.getContentPane().add(label_7);
		
		JLabel label_8 = new JLabel("id_producator");
		label_8.setForeground(Color.WHITE);
		label_8.setBounds(253, 292, 64, 14);
		frmTema.getContentPane().add(label_8);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(327, 186, 86, 20);
		frmTema.getContentPane().add(textField_12);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(327, 211, 86, 20);
		frmTema.getContentPane().add(textField_13);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(327, 236, 86, 20);
		frmTema.getContentPane().add(textField_14);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		textField_15.setBounds(327, 261, 86, 20);
		frmTema.getContentPane().add(textField_15);
		
		textField_16 = new JTextField();
		textField_16.setColumns(10);
		textField_16.setBounds(327, 289, 86, 20);
		frmTema.getContentPane().add(textField_16);
		
		JButton btnEditProdus = new JButton("Edit Produs");
		btnEditProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProdusBLL pbll = new ProdusBLL();
				Produs p = new Produs(Integer.parseInt(textField_12.getText()), textField_13.getText(),  Integer.parseInt(textField_14.getText()),  Integer.parseInt(textField_15.getText()), Integer.parseInt(textField_16.getText()));
				pbll.updateProdus(p);
			}
		});
		btnEditProdus.setBounds(253, 336, 128, 23);
		frmTema.getContentPane().add(btnEditProdus);
		
		JLabel label_9 = new JLabel("id_produs");
		label_9.setForeground(Color.WHITE);
		label_9.setBounds(457, 186, 64, 14);
		frmTema.getContentPane().add(label_9);
		
		textField_17 = new JTextField();
		textField_17.setColumns(10);
		textField_17.setBounds(531, 186, 86, 20);
		frmTema.getContentPane().add(textField_17);
		
		JButton btnDeleteProdus = new JButton("Delete Produs");
		btnDeleteProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProdusBLL pbll = new ProdusBLL();
				pbll.deleteProdus(Integer.parseInt(textField_17.getText()));
			}
		});
		btnDeleteProdus.setBounds(457, 211, 128, 23);
		frmTema.getContentPane().add(btnDeleteProdus);
		
		JButton btnViewProduse = new JButton("View Produse");
		btnViewProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewProduse wp = new ViewProduse();
			}
		});
		btnViewProduse.setBounds(457, 336, 128, 23);
		frmTema.getContentPane().add(btnViewProduse);
		
		JLabel lblIdorder_1 = new JLabel("id_order");
		lblIdorder_1.setForeground(Color.WHITE);
		lblIdorder_1.setBounds(33, 394, 64, 14);
		frmTema.getContentPane().add(lblIdorder_1);
		
		JLabel lblIdclient_2 = new JLabel("id_client");
		lblIdclient_2.setForeground(Color.WHITE);
		lblIdclient_2.setBounds(33, 419, 64, 14);
		frmTema.getContentPane().add(lblIdclient_2);
		
		JLabel lblIdprodus_1 = new JLabel("id_produs");
		lblIdprodus_1.setForeground(Color.WHITE);
		lblIdprodus_1.setBounds(33, 444, 64, 14);
		frmTema.getContentPane().add(lblIdprodus_1);
		
		JLabel lblCantitate_1 = new JLabel("cantitate");
		lblCantitate_1.setForeground(Color.WHITE);
		lblCantitate_1.setBounds(33, 469, 46, 14);
		frmTema.getContentPane().add(lblCantitate_1);
		
		textField_18 = new JTextField();
		textField_18.setColumns(10);
		textField_18.setBounds(107, 394, 86, 20);
		frmTema.getContentPane().add(textField_18);
		
		textField_19 = new JTextField();
		textField_19.setColumns(10);
		textField_19.setBounds(107, 419, 86, 20);
		frmTema.getContentPane().add(textField_19);
		
		textField_20 = new JTextField();
		textField_20.setColumns(10);
		textField_20.setBounds(107, 444, 86, 20);
		frmTema.getContentPane().add(textField_20);
		
		textField_21 = new JTextField();
		textField_21.setColumns(10);
		textField_21.setBounds(107, 469, 86, 20);
		frmTema.getContentPane().add(textField_21);
		
		JButton btnComanda = new JButton("Comanda");
		btnComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				OrderBLL obll = new OrderBLL();
				ProdusBLL prdbll = new ProdusBLL();
				Produs p = prdbll.findProdusById( Integer.parseInt(textField_20.getText()) );
				
				if(p.getCantitate() -  Integer.parseInt(textField_21.getText())<0)
				{
					JOptionPane.showMessageDialog(null,"Produse Insuficiente");	
				}
				else
				{
					p.setCantitate(p.getCantitate() -  Integer.parseInt(textField_21.getText()) );
					obll.insertOrder(new Order(Integer.parseInt(textField_18.getText()) , Integer.parseInt(textField_19.getText()), Integer.parseInt(textField_20.getText()), Integer.parseInt(textField_21.getText()) ));
					prdbll.updateProdus(p);
					BufferedWriter writer = null;
					try
					{
					    writer = new BufferedWriter( new FileWriter( "bill.txt"));
					    writer.write("The Client with the id "+Integer.parseInt(textField_19.getText())+" bought the product " + p.getNume()+" cantitate: "+Integer.parseInt(textField_21.getText()));

					}
					catch ( IOException e1)
					{
					}
					finally
					{
					        if ( writer != null)
								try {
									writer.close();
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
					}
				}
				
			}
		});
		btnComanda.setBounds(33, 506, 128, 23);
		frmTema.getContentPane().add(btnComanda);
	}
	
	public JFrame getFrame() {
		return frmTema;
	}
}
