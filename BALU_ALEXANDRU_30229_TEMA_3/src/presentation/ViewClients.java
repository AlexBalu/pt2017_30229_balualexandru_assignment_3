package presentation;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import bll.ClientBLL;
import model.Client;
import presentation.Reflection;

public class ViewClients extends JFrame {
	private JScrollPane s ;
	private JPanel p;
	
	public ViewClients(){
		p = new JPanel();
		init();
	}
	
	private void init(){
		ClientBLL clientBll = new ClientBLL();
		List<Client> client1 = new ArrayList<Client>();
		client1 = clientBll.selectAll();
		try {
			client1 = clientBll.selectAll();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		List<Object> c = new ArrayList<Object>();
		for(Client it : client1)
			c.add(it);
			
		Object[] atribute = { "id_client","nume","varsta"};
		s = new JScrollPane(Reflection. createJTable(c,atribute));
		p.add(s);
		this.add(p,BorderLayout.CENTER);
		this.setVisible(true);
		this.setSize(600, 600);
	}
}
