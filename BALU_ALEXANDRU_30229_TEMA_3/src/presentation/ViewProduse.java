package presentation;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import bll.ProdusBLL;
import model.Produs;

public class ViewProduse extends JFrame {
	private JScrollPane s ;
	private JPanel p;
	
	public ViewProduse(){
		s = new JScrollPane();
		p = new JPanel();
		init();
	}
	
	private void init(){
	ProdusBLL produsBll = new ProdusBLL();
	List<Produs> produs1 = new ArrayList<Produs>();
	produs1 = produsBll.selectAll();
	try {
			produs1 = produsBll.selectAll();
	} catch (Exception ex) {
		System.out.println(ex.getMessage());
	}
	List<Object> c = new ArrayList<Object>();
	for(Produs it : produs1)
		c.add(it);
		
	Object[] atribute = { "Id_Produs","Nume_Produs","Cantitate","Pret","Id_Producator"};
	s = new JScrollPane(Reflection.createJTable(c, atribute));
	
	p.add(s);
	this.add(p,BorderLayout.CENTER);
	this.setVisible(true);
	this.setSize(600, 600);
	}
}
