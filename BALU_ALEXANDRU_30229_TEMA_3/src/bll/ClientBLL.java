package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ClientDAO;
import model.Client;

public class ClientBLL {
	dao.ClientDAO cdao = new ClientDAO();
	//private List<Validator<Client>> validators;

	public ClientBLL()
	{
	    //validators = new ArrayList<Validator<Client>>();
		//validators.add(new ClientAgeValidator());
	}
	
	public Client findClientById(int id) {
		Client st = cdao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertClient(Client client) {
		return cdao.create(client);
	}
	
	public Client readClient(int id)
	{
	return cdao.read(id);
	}
	
	public boolean updateClient(Client c)
	{
		return cdao.update(c);
	}
	
	public boolean deleteClient(int id)
	{
		return cdao.delete(id);
	}
	
	public List<Client> selectAll() {
		List<Client> cl = new ArrayList<Client>();
		cl= cdao.selectAll();
		if (cl == null) {
			throw new NoSuchElementException("Elements were not found!");
		}
		return cl;
	}
	
	
}
