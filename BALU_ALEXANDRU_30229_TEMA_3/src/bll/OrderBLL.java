package bll;

import java.util.NoSuchElementException;

import dao.OrderDAO;
import model.Order;
import model.Order;

public class OrderBLL {
	dao.OrderDAO odao = new OrderDAO();
	
	public Order findOrderById(int id) {
		Order st = odao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertOrder(Order order) {
		return odao.create(order);
	}
	
	public Order readOrder(int id)
	{
	return odao.read(id);
	}
	
	public boolean updateOrder(Order c)
	{
		return odao.update(c);
	}
	
	public boolean deleteOrder(int id)
	{
		return odao.delete(id);
	}
}
