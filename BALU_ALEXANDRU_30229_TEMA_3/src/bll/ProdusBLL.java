package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ProdusDAO;
import model.Produs;

public class ProdusBLL {
	dao.ProdusDAO prsdao = new ProdusDAO();
	
	public Produs findProdusById(int id) {
		Produs st = prsdao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The produs with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertProdus(Produs produs) {
		return prsdao.create(produs);
	}
	
	public Produs readProdus(int id)
	{
	return prsdao.read(id);
	}
	
	public boolean updateProdus(Produs c)
	{
		return prsdao.update(c);
	}
	
	public boolean deleteProdus(int id)
	{
		return prsdao.delete(id);
	}
	
	 public List<Produs> selectAll()
	{
		List<Produs> p = prsdao.selectAll();
		if (p == null) {
			throw new NoSuchElementException("Elements were not found!");
		}
		return p;
	
	}
}
