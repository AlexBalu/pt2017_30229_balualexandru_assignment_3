package bll;

import java.util.NoSuchElementException;

import dao.ProducatorDAO;
import model.Producator;

public class ProducatorBLL {
	
		dao.ProducatorDAO pdao = new ProducatorDAO();
		
		public Producator findProducatorById(int id) {
			Producator st = pdao.findById(id);
			if (st == null) {
				throw new NoSuchElementException("The producator with id =" + id + " was not found!");
			}
			return st;
		}

		public int insertProducator(Producator producator) {
			return pdao.create(producator);
		}
		
		public Producator readProducator(int id)
		{
		return pdao.read(id);
		}
		
		public boolean updateProducator(Producator c)
		{
			return pdao.update(c);
		}
		
		public boolean deleteProducator(int id)
		{
			return pdao.delete(id);
		}
	
}
