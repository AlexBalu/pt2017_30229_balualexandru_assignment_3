package start;

import presentation.View;
import presentation.ViewApp;

public class Start {

	public static void main(String[] args) {
		
		//Client test//
		/*
		ClientBLL cbll = new ClientBLL();
		cbll.insertClient(new Client(5,"Gabi Balu",18));
		//verify is inserted
		Client c1 = cbll.findClientById(5);
		System.out.println("Client read: "+c1.getNume());
		
		//
		cbll.updateClient(new Client(5,"Gabi Ioan Balu",18));
		c1 = cbll.findClientById(5);
		System.out.println("Client updated read: "+c1.getNume());
		
		cbll.deleteClient(5);
		*/
		
		
		//Order test//
		/*
		OrderBLL obll = new OrderBLL();
		obll.insertOrder(new Order(3,4,1,5));
		//verify is inserted
		Order o1 = obll.findOrderById(3);
		System.out.println("Order cantitate read: "+o1.getCantitate());
		
		//
		obll.updateOrder(new Order(3,4,1,500));
		o1 = obll.findOrderById(3);
		System.out.println("Order updated read: "+o1.getCantitate());
		
		obll.deleteOrder(3);
		*/
		
		//Producator test
		/*
				ProducatorBLL pbll = new ProducatorBLL();
				pbll.insertProducator(new Producator(1,"HP",1));
				//verify is inserted
				Producator p1 = pbll.findProducatorById(1);
				System.out.println("producator nume read: "+p1.getNume_producator());
				
				//
				pbll.updateProducator(new Producator(1,"Hpe",101));
				p1 = pbll.findProducatorById(1);
				System.out.println("producator update nume read: "+p1.getNume_producator());
				
				pbll.deleteProducator(1);
		 */
		
		//Produs test
		/*
		ProdusBLL prdbll = new ProdusBLL();
		prdbll.insertProdus(new Produs(1,"Laptop HP",10,2300,1));
		//verify is inserted
		Produs prd1 = prdbll.findProdusById(1);
		System.out.println("produs nume read: "+prd1.getNume());
		
		//
		prdbll.updateProdus(new Produs(1,"Laptop HPe",10,2300,1));
		prd1 = prdbll.findProdusById(1);
		System.out.println("produs update nume read: "+prd1.getNume());
		
		prdbll.deleteProdus(1);
				*/
		ViewApp window = new ViewApp();
		window.getFrame().setVisible(true);
		//View theView = new View();
		
				
	}
}
